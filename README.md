![Build Status](https://gitlab.com/iop-optical-group/humans/badges/master/pipeline.svg)

---
This repository is for the Optical Group Humans website. 

The idea is to highlight the diversity of a) the humans within the optical group
and b) the diversity of the work. 

This is WIP at the moment. If you are intrested in getting involved
let Aaron Goodwin-Jones know. 

The website can be found at https://iop-optical-group.gitlab.io/humans

---

## Nominate a human
Please create a gitlab issue (TODO: Create issue template)

## Developing the website
Website development has three stages. First, make and test the 
changes locally on your computer. 

Then create a merge request.

Second, Aaron will test them on the development server and
circulate to the optical group.

Finnaly, your merge request will be approved and your changes
deployed automatically. 

### Working Locally
To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Install [Frozen-Flask](http://pythonhosted.org/Frozen-Flask/)
1. Run `app.py`

Read more at Frozen-Flask's [documentation](http://pythonhosted.org/Frozen-Flask/).

### Updating the public website
The public website is generated through a gitlab [project Pages][projpages].
See the gitlab docs for more details

---

Forked from https://gitlab.com/dAnjou/frozen-flask-for-gitlab-pages

[ci]: https://about.gitlab.com/gitlab-ci/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
